//
//  main.m
//  DesafioCep
//
//  Created by Lilian Dias on 11/08/14.
//  Copyright (c) 2014 Lilian Dias. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CSAppDelegate class]));
    }
}
