//
//  CSPesquisarCEPViewController.m
//  DesafioCep
//
//  Created by Lilian Dias on 11/08/14.
//  Copyright (c) 2014 Lilian Dias. All rights reserved.
//

#import "CSPesquisarCEPViewController.h"

@interface CSPesquisarCEPViewController ()

@end

@implementation CSPesquisarCEPViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Formato o UITextField para o padrão brasileiro de CEP
    self.txtCepDigitado.numericFormatter = [AKNumericFormatter formatterWithMask:@"**.***-***" placeholderCharacter:'*'];
    
    // Inicio o array de histórico de CEP baseado em arquivo local
    NSString *arquivoHistorico = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@", HISTORICO_CEP];
    _historicoCEP = [NSKeyedUnarchiver unarchiveObjectWithFile:arquivoHistorico];
    if (!_historicoCEP) {
        _historicoCEP = [NSMutableArray arrayWithCapacity:0];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"segue_verHistorico"]) {
        CSHistoricoCEPViewController *destination = [segue destinationViewController];
        destination.arrayHistorico = _historicoCEP;
    }
}

- (void)fecharHistorico:(UIStoryboardSegue *)segue{
    _txtCepDigitado.text = @"";
    _txtFeedBack.text = @"";
}

#pragma mark - Métodos da classe

- (void)consultarCEP:(id)sender{
    
    // Limpo o console de Feedback
    _txtFeedBack.text = @"";
    // Desapareço com o teclado
    [_txtCepDigitado resignFirstResponder];
    
    // Verifico se há algo difitado no CEP. Caso positivo limpo a máscara e faço um request GET à API
    if (![self verificarTextoDosCamposDeTexto:@[self.txtCepDigitado]] && _txtCepDigitado.text.length == 10) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        // Mostro o indicador de atividade e desabilito a interação do usuário com a view
        [_activityIndicator setHidden:NO];
        [_activityIndicator startAnimating];
        [self.view setUserInteractionEnabled:NO];
        [self limparMascara:self.txtCepDigitado];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSString *request = [NSString stringWithFormat:@"%@%@", WS_URL, _cepSemMascara];
        [manager GET:request parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            [_activityIndicator stopAnimating];
            [_activityIndicator setHidden:YES];
            [self.view setUserInteractionEnabled:YES];
            [self processarResposta:responseObject];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            [_activityIndicator stopAnimating];
            [_activityIndicator setHidden:YES];
            [self.view setUserInteractionEnabled:YES];
            NSHTTPURLResponse *erroResponse = [[error userInfo] objectForKey:@"com.alamofire.serialization.response.error.response"];
            switch ([erroResponse statusCode]) {
                case 404:
                {
                    _txtFeedBack.textColor = [UIColor redColor];
                    _txtFeedBack.text = @"Desculpe. \nO CEP que você digitou não foi encontrado.";
                    break;
                }
                case 400:
                {
                    _txtFeedBack.textColor = [UIColor redColor];
                    _txtFeedBack.text = @"Desculpe. \nHouve algum erro de comunicação com o servidor.";
                    break;
                }
                default:
                    break;
            }
        }];
    } else {
        _txtFeedBack.text = @"Desculpe. \nVocê deve digitar um CEP válido para consulta.";
        _txtCepDigitado.text = @"";
    }
}

- (void)limparMascara: (UITextField*) textField{
    if (textField == self.txtCepDigitado) {
        _cepSemMascara = [_txtCepDigitado.text substringWithRange:NSMakeRange(0, 2)];
        _cepSemMascara = [_cepSemMascara stringByAppendingString:[self.txtCepDigitado.text substringWithRange:NSMakeRange(3, 3)]];
        _cepSemMascara = [_cepSemMascara stringByAppendingString:[self.txtCepDigitado.text substringWithRange:NSMakeRange(7, 3)]];
    }
}

- (BOOL)verificarTextoDosCamposDeTexto: (NSArray*)arrayTextFields{
    BOOL possuiTextoVazio = NO;
    
    for (UITextField *esseCampoDeTexto in arrayTextFields) {
        if (esseCampoDeTexto.text.length == 0 || !esseCampoDeTexto) {
            possuiTextoVazio = YES;
            break;
        }
    }
    return possuiTextoVazio;
}

- (BOOL)elementoRepetido: (CSCEP*)cep{
    BOOL retorno = NO;
    for (CSCEP* cepNoArray in _historicoCEP) {
        if ([cepNoArray.numeroCep isEqualToString:cep.numeroCep]) {
            retorno = YES;
            break;
        }
    }
    return retorno;
}

- (void)processarResposta: (NSDictionary*) resposta{
    NSError *error;
    CSCEP *cep = [[CSCEP alloc]init];
    cep = [MTLJSONAdapter modelOfClass:[CSCEP class] fromJSONDictionary:resposta error:&error];
    if (!error) {
        _txtFeedBack.text = [NSString stringWithFormat:@"CEP: %@\n%@ %@\nBairro: %@\t%@ - %@", cep.numeroCep, cep.tipoDeLogradouro, cep.logradouro, cep.bairro, cep.cidade, cep.estado];
        if (![self elementoRepetido:cep]) {
            [_historicoCEP addObject:cep];
            // Salvo histórico em arquivo
            NSString *arquivoHistorico = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@", HISTORICO_CEP];
            if (![NSKeyedArchiver archiveRootObject:_historicoCEP toFile:arquivoHistorico]) {
                NSLog(@"Erro ao salvar: %@ \n no arquivo %@", _historicoCEP, arquivoHistorico);
            }
        }
    }else{
        NSLog(@"Erro [processarResposta]: %@", error);
        UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:@"Desculpe" message:@"Ocorreu um erro ao processar seu CEP. Por gentileza, tente novamente." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alerta show];
    }
    
}

#pragma mark - Touch Methods

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [_txtCepDigitado resignFirstResponder];
}
@end
