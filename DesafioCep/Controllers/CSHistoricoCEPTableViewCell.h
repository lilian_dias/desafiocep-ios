//
//  CSHistoricoCEPTableViewCell.h
//  DesafioCep
//
//  Created by Lilian Dias on 11/08/14.
//  Copyright (c) 2014 Lilian Dias. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSHistoricoCEPTableViewCell : UITableViewCell

@property (weak,nonatomic) IBOutlet UILabel *txtCEP;
@property (weak,nonatomic) IBOutlet UILabel *txtTipoDeLogradouro;
@property (weak,nonatomic) IBOutlet UILabel *txtLogradouro;
@property (weak,nonatomic) IBOutlet UILabel *txtBairro;
@property (weak,nonatomic) IBOutlet UILabel *txtCidade;
@property (weak,nonatomic) IBOutlet UILabel *txtEstado;

@end
