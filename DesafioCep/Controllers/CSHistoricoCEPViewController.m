//
//  CSHistoricoCEPViewController.m
//  DesafioCep
//
//  Created by Lilian Dias on 11/08/14.
//  Copyright (c) 2014 Lilian Dias. All rights reserved.
//

#import "CSHistoricoCEPViewController.h"

@interface CSHistoricoCEPViewController ()

@end

@implementation CSHistoricoCEPViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UINib *nib = [UINib nibWithNibName:@"CSHistoricoCEPTableViewCell" bundle:nil];
    [self.tabelaHistorico registerNib:nib forCellReuseIdentifier:@"historico_cell"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - TableView DataSource & Delegate Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([_arrayHistorico count] > 0) {
        return [_arrayHistorico count];
    } else {
        return 1;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([_arrayHistorico count] > 0) {
        CSHistoricoCEPTableViewCell *cell_CEP = [tableView dequeueReusableCellWithIdentifier:@"historico_cell" forIndexPath:indexPath];
        if (cell_CEP == nil) {
            cell_CEP = [[CSHistoricoCEPTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"historico_cell"];
        }
        
        // Preencho os UILabels da célula com os itens do array (CSCEP)
        CSCEP *cep = [_arrayHistorico objectAtIndex:indexPath.row];
        cell_CEP.txtCEP.text = cep.numeroCep;
        cell_CEP.txtTipoDeLogradouro.text = cep.tipoDeLogradouro;
        cell_CEP.txtLogradouro.text = cep.logradouro;
        cell_CEP.txtBairro.text = cep.bairro;
        cell_CEP.txtCidade.text = cep.cidade;
        cell_CEP.txtEstado.text = cep.estado;
        return cell_CEP;
    } else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"default_cell"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"default_cell"];
        }
        
        return  cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([_arrayHistorico count] > 0) {
        return 198;
    } else {
        return 44;
    }
}

@end
