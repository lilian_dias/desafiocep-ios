//
//  CSPesquisarCEPViewController.h
//  DesafioCep
//
//  Created by Lilian Dias on 11/08/14.
//  Copyright (c) 2014 Lilian Dias. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking.h>
#import <AKNumericFormatter.h>
#import <UITextField+AKNumericFormatter.h>
#import "Constantes.h"
#import "CSCEP.h"
#import "CSHistoricoCEPViewController.h"

@interface CSPesquisarCEPViewController : UIViewController

@property (strong, nonatomic) NSMutableArray *historicoCEP;
@property (strong, nonatomic) NSString *cepSemMascara;

@property (weak, nonatomic) IBOutlet UITextField *txtCepDigitado;
@property (weak, nonatomic) IBOutlet UITextView *txtFeedBack;
@property (weak, nonatomic) IBOutlet  UIActivityIndicatorView *activityIndicator;

- (IBAction)consultarCEP:(id)sender;
- (IBAction)fecharHistorico:(UIStoryboardSegue *)segue;
- (void)limparMascara: (UITextField*) textField;

@end
