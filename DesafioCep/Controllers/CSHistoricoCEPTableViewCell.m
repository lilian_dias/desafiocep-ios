//
//  CSHistoricoCEPTableViewCell.m
//  DesafioCep
//
//  Created by Lilian Dias on 11/08/14.
//  Copyright (c) 2014 Lilian Dias. All rights reserved.
//

#import "CSHistoricoCEPTableViewCell.h"

@implementation CSHistoricoCEPTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
