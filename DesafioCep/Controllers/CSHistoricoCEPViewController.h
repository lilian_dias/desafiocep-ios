//
//  CSHistoricoCEPViewController.h
//  DesafioCep
//
//  Created by Lilian Dias on 11/08/14.
//  Copyright (c) 2014 Lilian Dias. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSHistoricoCEPTableViewCell.h"
#import "CSCEP.h"

@interface CSHistoricoCEPViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong,nonatomic) NSMutableArray *arrayHistorico;

@property (strong, nonatomic) IBOutlet UITableView *tabelaHistorico;

@end
