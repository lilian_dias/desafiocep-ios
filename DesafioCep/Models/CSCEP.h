//
//  CSCEP.h
//  DesafioCep
//
//  Created by Lilian Chaves Dias on 12/08/14.
//  Copyright (c) 2014 Lilian Dias. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface CSCEP : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSString *bairro;
@property (nonatomic, copy, readonly) NSString *numeroCep;
@property (nonatomic, copy, readonly) NSString *cidade;
@property (nonatomic, copy, readonly) NSString *estado;
@property (nonatomic, copy, readonly) NSString *logradouro;
@property (nonatomic, copy, readonly) NSString *tipoDeLogradouro;

@end
