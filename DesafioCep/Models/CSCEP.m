//
//  CSCEP.m
//  DesafioCep
//
//  Created by Lilian Chaves Dias on 12/08/14.
//  Copyright (c) 2014 Lilian Dias. All rights reserved.
//

#import "CSCEP.h"

@implementation CSCEP

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"bairro": @"bairro",
             @"numeroCep": @"cep",
             @"cidade": @"cidade",
             @"estado": @"estado",
             @"logradouro": @"logradouro",
             @"tipoDeLogradouro" : @"tipoDeLogradouro"
             };
}

@end
