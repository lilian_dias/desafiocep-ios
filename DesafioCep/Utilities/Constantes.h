//
//  Constantes.h
//  DesafioCep
//
//  Created by Lilian Dias on 11/08/14.
//  Copyright (c) 2014 Lilian Dias. All rights reserved.
//

#ifndef DesafioCep_Constantes_h
#define DesafioCep_Constantes_h

#define WS_URL @"http://correiosapi.apphb.com/cep/"
#define HISTORICO_CEP @"HistoricoCepBuscados"

#endif
