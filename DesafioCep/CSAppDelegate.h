//
//  CSAppDelegate.h
//  DesafioCep
//
//  Created by Lilian Dias on 11/08/14.
//  Copyright (c) 2014 Lilian Dias. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
