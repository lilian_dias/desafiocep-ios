//
//  DesafioCepTests.m
//  DesafioCepTests
//
//  Created by Lilian Dias on 11/08/14.
//  Copyright (c) 2014 Lilian Dias. All rights reserved.
//

#define EXP_SHORTHAND

#import <OCMock/OCMock.h>
#import "Specta.h"
#import "Expecta.h"
#import "CSPesquisarCEPViewController.h"
#import "CSHistoricoCEPViewController.h"

SpecBegin(DesafioTest)

describe(@"CSPesquisarCEPViewController", ^{
    __block CSPesquisarCEPViewController *vc;
    
    beforeEach(^{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"CSPesquisarCEPViewController"];
        [vc loadView];
        UIView *view = vc.view;
        expect(view).toNot.beNil;
    });
    
    it(@"limpar mascara do CEP digitado", ^{
        vc.txtCepDigitado.text = @"66.073-005";
        [vc limparMascara:vc.txtCepDigitado];
        expect(vc.cepSemMascara).to.equal(@"66073005");
    });
    
    it(@"request com CEP vazio", ^{
        vc.txtCepDigitado.text = @"";
        [vc consultarCEP:nil];
        expect(vc.txtFeedBack.text).to.equal(@"Desculpe. \nVocê deve digitar um CEP válido para consulta.");
    });
    
    it(@"request com CEP incompleto", ^{
        vc.txtCepDigitado.text = @"66000";
        [vc consultarCEP:nil];
        expect(vc.txtFeedBack.text).to.equal(@"Desculpe. \nVocê deve digitar um CEP válido para consulta.");
    });
});

describe(@"CSHistoricoCEPViewController", ^{
    
    __block CSHistoricoCEPViewController *hvc;
    
    beforeEach(^{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        hvc = [storyboard instantiateViewControllerWithIdentifier:@"CSHistoricoCEPViewController"];
        [hvc loadView];
        UIView *view = hvc.view;
        expect(view).toNot.beNil;
    });
    
    it(@"Historico nao deve ser nulo", ^{
        expect(hvc).toNot.beNil;
        expect(hvc).to.beInstanceOf([CSHistoricoCEPViewController class]);
    });    
    
    it(@"tabela historico deve ter delegate e datasource", ^{
        expect(hvc.tabelaHistorico.delegate).toNot.beNil;
        expect(hvc.tabelaHistorico.delegate).to.equal(hvc);
        expect(hvc.tabelaHistorico.dataSource).toNot.beNil;
        expect(hvc.tabelaHistorico.dataSource).to.equal(hvc);
    });
    
});

SpecEnd

